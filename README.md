# NixOS Configuration for COVIS-2

Custom NixOS configuration for the COVIS-2 System Interface
Controller. Adapted from the configuration for ARMS and IVAR.

## Prerequisites

See the [NixOS Manual](https://nixos.org/nixos/manual/) for the basic
installation instructions.

+ A minimal NixOS installation with `git` installed
+ Use `fdisk` to partition the external "data" disk (`/dev/sdb`).
+ Create a filesystem on the external disk and label it `covisdata`:

``` shellsession
mkfs.ext4 -L covisdata /dev/sdb1
```

## Installation

``` shellsession
mv /etc/nixos /etc/nixos.dist
git clone https://bitbucket.org/uwaploe/nixos-config.git /etc/nixos
cp /etc/nixos.dist/hardware-configuration.nix /etc/nixos/
ln -sr /etc/nixos/config.nix /etc/nixos/configuration.nix
```
