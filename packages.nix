{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gptfdisk
    wget
    chrony
    which
    emacs25-nox
    vim
    tmux
    fping
    gitAndTools.gitFull
    socat
    gcc
    gnumake
    pciutils
    kermit
    attr
    samba
    lsof
    docker_compose
    redis
    tcpdump
    arp-scan
  ];

}
