{ config, lib, pkgs, ... }:

{
  nixpkgs.config = {
    packageOverrides = pkgs: {
      inherit (pkgs.callPackages ./pkgs/default.nix { })
        daq1278;
    };
  };
}