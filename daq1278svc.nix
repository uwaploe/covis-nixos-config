{ config, lib, pkgs, ... }:
let
  cfg = config.services.daq1278;
in {
  options = {
    services.daq1278 = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = ''
          Whether to enable `daq1278' gRPC service.
        '';
      };

      address = lib.mkOption {
        default = ":10000";
        description = ''
          Address:port on which the gRPC server will listen for connections.
        '';
        type = lib.types.string;
      };

      ioaddr = lib.mkOption {
        default = 352;
        description = ''
          Base address of DAQ-1278 board (decimal integer).
        '';
        type = lib.types.int;
      };
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ pkgs.daq1278 ];

    systemd.services.daq1278 = {
      description = "gRPC server for DAQ-1278 DIO board";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = ''
          ${pkgs.daq1278}/bin/daq1278_svc -address=${cfg.address} -ioaddr=${toString cfg.ioaddr}
        '';
        Restart = "always";
      };
    };
  };
}
