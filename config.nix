# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:
let
  prj = "covis";
  datamount = "/data";
in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./packages.nix
      ./localpkgs.nix
      ./loginctl-linger.nix
      ./startchrony.nix
      ./fixlockdir.nix
      ./daq1278svc.nix
    ];

  fileSystems = lib.mkIf (datamount != null) {
    "${datamount}" = {
      device = "/dev/disk/by-label/${prj}data";
      fsType = "ext4";
    };
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = pkgs.linuxPackages;
  # Enable serial console for Linux
  boot.kernelParams = ["console=tty1" "console=ttyS0,115200n8"];

  boot.initrd.checkJournalingFS = false;

  # Sysctl settings
  boot.kernel.sysctl."kernel.panic" = 30;

  # Load driver for Moxa multi-port serial board
  boot.kernelModules = [
    "mxser"
  ];

  networking = {
    hostName = "covis-sic-1";
    firewall.enable = false;
    # interfaces.enp4s0.ip4 = [
    #   { address = "10.13.0.10"; prefixLength = 24; }
    # ];
    # defaultGateway = "<my gateway>";
    interfaces.enp5s0.ip4 = [
      { address = "10.136.104.137"; prefixLength = 24; }
    ];
    extraHosts = ''
    10.136.104.138 covis-sonar-nc
    '';
  };

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "UTC";
  services.ntp.enable = false;
  services.chrony.enable = true;
  services.chrony.servers = [
    "time1.u.washington.edu"
  ];
  # This service will check for an internet connection and,
  # if it's available, switch chronyd to online mode.
  # services.startchrony.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Crond
  services.cron.enable = true;

  # Docker
  virtualisation.docker.enable = true;
  virtualisation.docker.enableOnBoot = true;

  # SMB file sharing
  services.samba.enable = true;
  services.samba.syncPasswordsByPam = true;
  services.samba.shares = lib.mkIf (datamount != null) {
    covis = {
      path = "${datamount}";
      "read only" = "yes";
      browseable = "yes";
      "write list" = "sysop";
      "create mask" = 0644;
      "directory mask" = 0755;
    };
  };
  services.samba.extraConfig = ''
    workgroup = ${lib.toUpper prj}
    hosts allow = 127.0.0.1 10.95.97.0/24 10.0.97.0/24
  '';

  services.atd.enable = true;
  services.atd.allowEveryone = true;

  # Fix permissions on /run/lock for Kermit
  services.fixlockdir.enable = true;

  # DAQ-1278 gRPC server
  services.daq1278.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.sysop = {
    isNormalUser = true;
    uid = 1000;
    description = "System Operator";
    extraGroups = ["wheel" "users" "dialout" "docker"];
    linger = true;
  };

  # Allow the sysop account to use real-time scheduling priorities.
  security.pam.loginLimits = [
    { domain = "sysop"; item = "rtprio"; type = "hard"; value = "99"; }
    { domain = "sysop"; item = "rtprio"; type = "soft"; value = "99"; }
  ];

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  nix.trustedUsers = ["root" "sysop"];

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.09";

}
